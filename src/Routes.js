import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Cards from './components/Cards'
import CoursesOverview from './components/CoursesOverview'
import LoginModal from './components/LoginModal'
import Paygateway from './components/Paygateway'
import SignUp from './components/SignUp'

 
const Routes = () => {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Cards} />
                    <Route exact path="/courseoverview/:id" component={CoursesOverview} />
                   <Route exact path="/paygateway" component={Paygateway} />
                    <Route exact path="/login" component={LoginModal} />
                    <Route exact path="/signup" component={SignUp} />
                </Switch>
            </BrowserRouter>
        </div>
    )
}

export default Routes
