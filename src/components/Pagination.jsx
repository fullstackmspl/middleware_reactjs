import React from 'react'
import arrowpagin from '../assets/arrow-pagin.png'

const Pagination = () => {
    return (
        <div>
            <div className="courses-nextpages">
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item">
                            <a className="page-link clr-chng" href="color" aria-label="Previous">
                                <span aria-hidden="true"><img className="img-rotate" src={arrowpagin} alt=""/></span>
                                <span className="sr-only">Previous</span>
                            </a>
                        </li>
                        <li className="page-item"><a className="page-link" href="pagination-image">1 of 1</a></li>
                        <li className="page-item">
                            <a className="page-link clr-chng" href="pagination" aria-label="Next">
                                <span aria-hidden="true"><img src={arrowpagin} alt="" /></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    )
}

export default Pagination

