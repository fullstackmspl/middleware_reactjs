import React, {useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import './coursesoverview.css'
import course1 from '../assets/course1.png'
import filter2 from '../assets/filter2.png'
import filter1 from '../assets/filter1.png'
import { useParams } from 'react-router'

const CoursesOverview = () => {
    const history = useHistory();
    const { id } = useParams();
    const [productsById, setProductByID] = useState([]);
    // const [courseDetails, setCourseDetails] = useState({})

    const buyCourse = (e) => {
        e.preventDefault();
        history.push("/paygateway")
    }
    useEffect(() => {
        getProductById();
    }, []);

    const getProductById = async () => {
        try {
            let result = await fetch(`http://93.188.167.68:9001/productsById?id=${id}`, {
                method: "GET",
                headers:
                {
                    "Content-Type": "application/json"


                },
            }, );

            const productData = await result.json();

            if (productData.products && productData.products.length) {
                setProductByID(productData.products[0]);
                //     console.log("products",productData.products[0].product);
                // console.log("pructid",productsById);
            }


        } catch (err) {
            alert("error-" + err)
        }

    }

    // useEffect(() => {
    //     getCourseDetails();
    // }, []);

    // const getCourseDetails = async () => {
    //     try {
    //         let result = await fetch(`http://kornferrysb-api.sabacloud.com/v1/offering?type=instructor_led`, {
    //             method: "GET",
    //             headers:
    //             {
    //                 "access-control-allow-origin" : "*",
    //                 "Content-Type": "application/json",
    //                 "SabaCertificate": "TkEzVDFTTkIwMTgyXiNeSlNueWlLcWVPRm0zUWRXa0N1NGRLTFotSVdWZ19GelZMQkdvcHR6WWJScl9GcUN3cTZlM28yNHg3QjRYbU14V0pyNFZzUjZhUmdRT0pYZEtZdkxDSGNlZV9GWFIzcmF6X1lHckZuTEVGNkhUdGgwS0xpLTdhd2FCUV82STZVT3VyaHNtVUVWNnhvazdDYjNuRjBJbmpB"
    //             },
    //         }, []);

    //         const details = await result.json();
    //         setCourseDetails(details.results)
    //     } catch (err) {
    //         alert("error-" + err)
    //     }
    // }


    return (
        <div>

            <div className="course-header">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <h1>OUR PRODUCTS</h1>
                        </div>
                        <div className="col-md-3">
                            <h2>ELEARNNING LEADERSHIP ARCHITECT</h2>
                        </div>
                        <div className="col-md-3">
                            <h3>TALENT ACQUISITION</h3>
                        </div>
                        <div className="col-md-3">
                            <h4>TALENT MANAGEMENT</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div className="course-detail">
                <div className="container">
                    <div className="row course-color">
                        <div className="col-md-6 course-detail2 ">
                            <img src={course1} alt="" />
                        </div>
                        <div className="col-md-6 course-detail1 ">
                            <h1>
                                Course: {productsById?.display?.en}
                            </h1>
                            <h2>
                                Strategic Selling Certification
                            </h2>
                            <h3>
                                Course ID:-{productsById?.taxcode}
                            </h3>
                            <h3>
                                Price:-{productsById?.pricing?.price?.USD}
                            </h3>
                            <div>
                                <button className="btn-1">
                                    Not Registerred
                                </button>
                            </div>
                            <div>
                                <button className="btn-2">
                                    SEE CLASSES TO ENROLL
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="row course-deatailpart course-color">
                        <div className="col-md-6 ">
                            <div className="course-detail3">
                                <h1>
                                    COURSE OVERVIEW
                                </h1>
                                <hr />
                                <h2>
                                    AVAILABLE CLASSES
                                </h2>
                            </div>


                        </div>
                        <div className="col-md-6 course-detail4">
                            <h1>
                                Description
                            </h1>
                            <h2>
                                {productsById?.taxcodeDescription}
                            </h2>
                        </div>
                    </div>

                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">

                        </div>
                        <div className="col-md-6">
                            <div className=" course-buy">
                                <div className="flex-coursebuy">
                                    <h1>Available classes(4)<span><img src={filter2} alt="" /> </span> </h1>
                                    <h3>
                                        Filters <span><img src={filter1} alt="" /></span>
                                    </h3>
                                </div>

                                <div className="course-buybox">
                                    <div>
                                        <h2>19-AUG-2021 - 19-AUG-2021<br />
                                            {productsById?.pricing?.price?.USD}<br />
                                            {/* <h3>Id: {courseDetails?.id}</h3> */}
                                            Session Details: 19-AUG-2021<br />
                                            | 2:00 AM - 11:00 AM (EDT)<br />
                                            <span className="buybox-1">English  &nbsp;|Virtual Class &nbsp;| Class ID: 0000013050<br />
                                                Albania,Tirana,Tirana <br />
                                                Total duration: 09:00 Hrs|&nbsp; &nbsp; &nbsp; &nbsp; <span className="buybox-2">Check cancellation policy</span> </span><br />
                                            <span className="buybox-3">27 seats available| &nbsp; &nbsp; &nbsp; &nbsp; 0 Waitlist available</span> </h2>
                                    </div>

                                    <div className="buyboxlower">
                                        <div>
                                            <img src={filter2} alt="" />
                                        </div>
                                        <div>
                                            <button onClick={buyCourse}>BUY NOW</button>
                                            <span><img src={filter2} alt="" /></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" course-buy">
                                <div className="flex-coursebuy">
                                    <h1>Available classes(4)<span><img src={filter2} alt="" /> </span> </h1>
                                    <h3>
                                        Filters <span><img src={filter1} alt="" /></span>
                                    </h3>
                                </div>

                                <div className="course-buybox">
                                    <div>
                                        <h2>19-AUG-2021 - 19-AUG-2021<br />
                                            {productsById?.pricing?.price?.USD}<br />
                                            Session Details: 19-AUG-2021<br />
                                            | 2:00 AM - 11:00 AM (EDT)<br />
                                            <span className="buybox-1">English  &nbsp;|Virtual Class &nbsp;| Class ID: 0000013050<br />
                                                Albania,Tirana,Tirana <br />
                                                Total duration: 09:00 Hrs|&nbsp; &nbsp; &nbsp; &nbsp; <span className="buybox-2">Check cancellation policy</span> </span><br />
                                            <span className="buybox-3">27 seats available| &nbsp; &nbsp; &nbsp; &nbsp; 0 Waitlist available</span> </h2>
                                    </div>

                                    <div className="buyboxlower">
                                        <div>
                                            <img src={filter2} alt="" />
                                        </div>
                                        <div>
                                            <button onClick={buyCourse}>BUY NOW</button>
                                            <span><img src={filter2} alt="" /></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" course-buy">
                                <div className="flex-coursebuy">
                                    <h1>Available classes(4)<span><img src={filter2} alt="" /> </span> </h1>
                                    <h3>
                                        Filters <span><img src={filter1} alt="" /></span>
                                    </h3>
                                </div>

                                <div className="course-buybox">
                                    <div>
                                        <h2>19-AUG-2021 - 19-AUG-2021<br />
                                            {productsById?.pricing?.price?.USD}<br />
                                            Session Details: 19-AUG-2021<br />
                                            | 2:00 AM - 11:00 AM (EDT)<br />
                                            <span className="buybox-1">English  &nbsp;|Virtual Class &nbsp;| Class ID: 0000013050<br />
                                                Albania,Tirana,Tirana <br />
                                                Total duration: 09:00 Hrs|&nbsp; &nbsp; &nbsp; &nbsp; <span className="buybox-2">Check cancellation policy</span> </span><br />
                                            <span className="buybox-3">27 seats available| &nbsp; &nbsp; &nbsp; &nbsp; 0 Waitlist available</span> </h2>
                                    </div>

                                    <div className="buyboxlower">
                                        <div>
                                            <img src={filter2} alt="" />
                                        </div>
                                        <div>
                                            <button onClick={buyCourse}>BUY NOW</button>
                                            <span><img src={filter2} alt="" /></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" course-buy">
                                <div className="flex-coursebuy">
                                    <h1>Available classes(4)<span><img src={filter2} alt="" /> </span> </h1>
                                    <h3>
                                        Filters <span><img src={filter1} alt="" /></span>
                                    </h3>
                                </div>

                                <div className="course-buybox">
                                    <div>
                                        <h2>19-AUG-2021 - 19-AUG-2021<br />
                                            {productsById?.pricing?.price?.USD}<br />
                                            Session Details: 19-AUG-2021<br />
                                            | 2:00 AM - 11:00 AM (EDT)<br />
                                            <span className="buybox-1">English  &nbsp;|Virtual Class &nbsp;| Class ID: 0000013050<br />
                                                Albania,Tirana,Tirana <br />
                                                Total duration: 09:00 Hrs|&nbsp; &nbsp; &nbsp; &nbsp; <span className="buybox-2">Check cancellation policy</span> </span><br />
                                            <span className="buybox-3">27 seats available| &nbsp; &nbsp; &nbsp; &nbsp; 0 Waitlist available</span> </h2>
                                    </div>

                                    <div className="buyboxlower">
                                        <div>
                                            <img src={filter2} alt="" />
                                        </div>
                                        <div>
                                            <button onClick={buyCourse}>BUY NOW</button>
                                            <span><img src={filter2} alt="" /></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className=" course-buy">
                                <div className="flex-coursebuy">
                                    <h1>Available classes(4)<span><img src={filter2} alt="" /> </span> </h1>
                                    <h3>
                                        Filters <span><img src={filter1} alt="" /></span>
                                    </h3>
                                </div>

                                <div className="course-buybox">
                                    <div>
                                        <h2>19-AUG-2021 - 19-AUG-2021<br />
                                            {productsById?.pricing?.price?.USD}<br />
                                            Session Details: 19-AUG-2021<br />
                                            | 2:00 AM - 11:00 AM (EDT)<br />
                                            <span className="buybox-1">English  &nbsp;|Virtual Class &nbsp;| Class ID: 0000013050<br />
                                                Albania,Tirana,Tirana <br />
                                                Total duration: 09:00 Hrs|&nbsp; &nbsp; &nbsp; &nbsp; <span className="buybox-2">Check cancellation policy</span> </span><br />
                                            <span className="buybox-3">27 seats available| &nbsp; &nbsp; &nbsp; &nbsp; 0 Waitlist available</span> </h2>
                                    </div>

                                    <div className="buyboxlower">
                                        <div>
                                            <img src={filter2} alt="" />
                                        </div>
                                        <div>
                                            <button onClick={buyCourse}>BUY NOW</button>
                                            <span><img src={filter2} alt="" /></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    )
}

export default CoursesOverview
