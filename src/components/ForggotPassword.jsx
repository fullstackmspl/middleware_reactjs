import React from 'react'
import loginmodal from '../assets/login-modal.png'
import logo2 from '../assets/logo2.png'
import './loginmodal.css'

const ForggotPassword = () => {
    return (
        <div>
            <div className="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="forgot-modal" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <img className="login-img1" src={loginmodal} alt=""/>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <div className="login-content">
                                <div className="login-content-img">
                                    <img src={logo2} alt=""/>
                                    <h2>Please create a new account here</h2>
                                </div>

                                <div className="login-content-form">
                                    <h1>Welcome to our new learning system! </h1>
                                    <form>
                                       
                                        <div className="form-group">
                                            <input type="email" 
                                            className="form-control" 
                                            placeholder="Email address" 
                                             />
                                        </div>
                                        
                                    </form>
                                </div>

                                <div className="login-content-buttons">
                                    <div>
                                        <button>PROCEED</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ForggotPassword
