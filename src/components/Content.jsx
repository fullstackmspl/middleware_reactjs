import React from 'react'
import './content.css'
import banner2 from '../assets/banner2.png'
import filtersearch from '../assets/filter-search.png'
import squarefilter from '../assets/square-filter.png'
import linesfilter from '../assets/lines-filter.png'
import filter1 from '../assets/filter1.png'

const Content = () => {
    return (
        <div>
            <div className="course-header">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <h1>OUR PRODUCTS</h1>
                        </div>
                        <div className="col-md-3">
                            <h2>ELEARNNING LEADERSHIP ARCHITECT</h2>
                        </div>
                        <div className="col-md-3">
                            <h3>TALENT ACQUISITION</h3>
                        </div>
                        <div className="col-md-3">
                            <h4>TALENT MANAGEMENT</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div className="banner-image">
                <img src={banner2} className="img-fluid" alt="" />
            </div>

            <div className="filter-view">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 filter-views">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="dropdown-rel">
                                        <form className="form-inline ">
                                            <input className="form-control" type="search" />
                                        </form>
                                       
                                        <button className="btn filter-search" data-toggle="collapse" href="#filter-searchcollapse" role="button" aria-expanded="false" aria-controls="filter-searchcollapse">
                                        <img src={filtersearch} alt="" />
                                        </button>
                                        <div className="srarch-filtercollapse collapse" id="filter-searchcollapse">
                                            <h1>filters</h1>
                                            <h1>price: Low to high</h1>
                                            <h1>price: high to low</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <div className="view-filter">
                                        <span>View</span>
                                        <div className="view-filter-images">
                                            <img src={squarefilter} alt="square-filter" />
                                            &nbsp;
                                            |
                                            &nbsp;
                                            <img src={linesfilter} alt="lines-filter" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="select-filter">
                                        <h1>
                                            PER PAGE
                                        </h1>
                                        <select className="form-select" aria-label="Default select example">
                                            <option selected>16</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                        <img src={filter1} alt="" />
                                    </div>

                                </div>
                                <div className="col-md-3">
                                    <div className="items-view">
                                        <span>(99 Items)</span>
                                        &nbsp;
                                        |
                                        &nbsp;
                                        <span className="cursor">View All</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Content
