import React, { useState, useEffect } from 'react'
import logo from '../assets/logo.png'
import searchicon from '../assets/search-icon.png'
import avatar from '../assets/avatar.png'

const Header = () => {

    // const account = JSON.parse((localStorage.getItem("userInfo")))
    // const accountId = account.id

    // const [accountInfo, setAccountInfo] = useState({})

    // useEffect(() => {
    //     getProfile();
    // }, []);

    // const getProfile = async () => {
    //     try {
    //         let result = await fetch(`http://93.188.167.68:9001/accountsById?id=${accountId}`, {
    //             method: "GET",
    //             headers:
    //             {
    //                 "Content-Type": "application/json"
    //             },
    //         }, )

    //         const accountData = await result.json();
    //         setAccountInfo(accountData.contact)
    //     }
    //     catch (err) {
    //         alert("error-" + err)
    //     }
    // }


    return (
        <div>
            <header>
                <div className="header">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <nav className="navbar navbar-expand-lg navbar-light">
                                    <a className="navbar-brand" href="/">
                                        <img src={logo} alt="" />
                                    </a>
                                    <button className="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"></span>
                                    </button>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <form className="form-inline ">
                                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                            <button className="btn search my-2" type="submit">
                                                <img src={searchicon} alt="" />
                                            </button>

                                             {/* <div className="avatar">
                                                    <img src={avatar} alt=""/>
                                                    <h1>hi ,{accountInfo.first}</h1>
                                                </div> */}
                                                    
                                                    <div className="avatar">
                                                        <img src={avatar} alt=""/>
                                                        <h1>Hi , FastSpring</h1>
                                                    </div>
                                            


                                        </form>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    )
}

export default Header
