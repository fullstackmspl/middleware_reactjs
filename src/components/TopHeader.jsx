import React from 'react';
import './topheader.css';


const TopHeader = () => {

   
   
    return (
        <div>
            <div className="top-header">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">

                        </div>
                        <div className="col-md-6 top-head-links">
                            <p><a href="Register">Register as Candidate </a></p>
                            <p><a href="certification">Certification</a></p>
                            <p><a href="event">Event </a></p>
                            <p><a href="meeting">Meeting</a></p>
                        </div>
                        <div className="col-md-2 login-link">
                            <p><a href="/login" data-toggle="modal" data-target="#login-modal">SignUp</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TopHeader
