import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import landing1 from '../assets/landing1.png'
import './cards.css'
import Content from './Content'
import Pagination from './Pagination'
import Footer from './Footer'



const Cards = () => {

    const history = useHistory();

    const buyProduct = () => {
        history.push(`/paygateway`)
    }

    const courseDetails = (id) => {
        history.push(`/courseoverview/${id}`)
    }

    const [productDetail, setProductDetail] = useState([])
    useEffect(() => {
        getProducts();
    }, []);

    const getProducts = async () => {
        try {
            let result = await fetch("http://93.188.167.68:9001/products/list", {
                method: "GET",
                headers:
                {
                     "Content-Type": "application/json"
                    

                },
            }, []);
            let productData = await result.json();
            setProductDetail(productData.products)
        } catch (err) {
            alert("error-" + err)
        }
    }


    // const imagesdata = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    return (
        <div>
            <Content />
            <div className="courses-library">
                <div className="container">
                    <div className="row courses-librarys">
                        {productDetail.map(item => (
                            <div className="col-md-4">
                                <div className="card">
                                    <img className="card-img-top" src={landing1} alt="" onClick={() => courseDetails(item)} />
                                    <div className="card-body">
                                        <p className="card-text">{item}</p>
                                        <p className="card-text-price"></p>
                                    </div>
                                    <div className="card-footer">
                                        <button onClick={()=>buyProduct(item)}>Buy Now</button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
            <Pagination />
            <Footer />
        </div>
    )
}

export default Cards
