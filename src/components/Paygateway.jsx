import React from 'react'
import './paygateway.css'
import filter1 from '../assets/filter1.png'
import ggprofile from '../assets/gg_profile.png'


const Paygateway = () => {
   
    return (

        <div>
            <div className="payment">
                <div className="container">
                    <div className="row payment-gate mt-5">
                        <div className="col-md-12">
                            <div className="review-head">
                                <h1>Review</h1>
                                <h1>Order</h1>
                            </div>
                        </div>
                    </div>
                    <div className="row payment-gateway">
                        <div className="col-md-8">
                            <div className="billing-address">
                                <div className="billing-head">
                                    <h1>Billing Address
                                        <span className="float-right" data-toggle="collapse" href="#Billing-Address" role="button" aria-expanded="false" aria-controls="Billing-Address">
                                            <img src={filter1} alt="filter1"/>
                                        </span>
                                    </h1>

                                </div>
                                <div className="billing-addressdata collapse show" id="Billing-Address">
                                    <h2>Billing Address</h2>
                                    <form>
                                        <div className="form-group">
                                            <input type="type" className="form-control" placeholder="Address line1" />
                                            <span><input type="type" className="form-control" placeholder="Address line2" /></span>
                                        </div>
                                        <div className="form-group">
                                            <input type="type" className="form-control" placeholder="Address line3" />
                                            <span><input type="type" className="form-control" placeholder="Country" /></span>
                                        </div>
                                        <div className="form-group">
                                            <input type="type" className="form-control" placeholder="State" />
                                            <span><input type="type" className="form-control" placeholder="City" /></span>
                                        </div>
                                        <div className="form-group">
                                            <input type="type" className="form-control" placeholder="Zip code" />
                                            <span><input type="type" className="form-control" placeholder="Email" /></span>
                                        </div>
                                    </form>

                                    <div className="btn-float">
                                        <button>CANCEL</button>
                                        <button>UPDATE</button>
                                    </div>
                                </div>
                            </div>
                            <div className="billing-addressdata">
                                <div className="billing-coupon">
                                    <h2>
                                        Coupon Code
                                    </h2>
                                    <div className="form-coupon">
                                        <form>
                                            <div className="form-group">
                                                <input type="type" className="form-control" placeholder="Enter promo code " />
                                                <button>
                                                    Apply
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="order-summary">
                                <div className="ordersummaryhea">
                                    <h1>
                                        Order Summary
                                    </h1>
                                </div>
                                <div className="item-totalmargin">
                                    <h2>
                                        item Total <span>1125.00</span>
                                    </h2>
                                    <h2>
                                        Order Total (USD) <span>1125.00</span>
                                    </h2>
                                </div>

                            </div>
                            <div className="complete-orderbnt">
                                <button>
                                    COMPLETE ORDER
                                </button>
                            </div>
                            <div>
                                <div className="cancel-orderbtn">
                                    <button >
                                        CANCEL
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row payment-gateway mt-5">
                        <div className="col-md-8">
                            <div className="billing-address">
                                <div className="billing-head">
                                    <h1>Item Details
                                        <span className="float-right" data-toggle="collapse" href="#Item-Details" role="button" aria-expanded="false" aria-controls="Item-Details">
                                            <img src={filter1} alt="filter"/>
                                        </span>
                                    </h1>
                                </div>
                                <div className="collapse show" id="Item-Details">


                                    <div className="billing-addressdata billing-Pricebox" >
                                        <h1>Learning</h1>
                                        <h1>Unit Price</h1>
                                        <h1>Total Price</h1>
                                    </div>
                                    <div className="billing-addressdata virtual-data">
                                        <h1><img src={ggprofile} alt="ggprofile"/> Strategic Selling Certification</h1>

                                        <h6>
                                            Virtual Class </h6>
                                        <h6>
                                            Duration: 09:00 </h6>
                                        <h6>
                                            Start Date: 19-AUG-2021 </h6>
                                        <h6>
                                            Location: Albania,Tirana,Tirana </h6>
                                        <h6>
                                            Language: English </h6>

                                    </div>
                                </div>
                            </div>

                            <div className="billing-address">
                                <div className="billing-head">
                                    <h1>Payment Method
                                        <span className="float-right" data-toggle="collapse" href="#Payment-Method" role="button" aria-expanded="false" aria-controls="Payment-Method">
                                            <img src={filter1} alt="filter"/>
                                        </span>
                                    </h1>
                                </div>
                                <div className="billing-addressdata billing-card collapse show" id="Payment-Method">
                                    <div>
                                        <button>
                                            Credit Card
                                            <span className="float-right" data-toggle="collapse" href="#Credit-Card" role="button" aria-expanded="false" aria-controls="Credit-Card">
                                                <img src={filter1} alt="filter"/>
                                            </span>
                                        </button>
                                        <div className="creditcardbtn collapse" id="Credit-Card">
                                            <h1>
                                                Pay by Credit card
                                            </h1>
                                            <h2>
                                                You have selected credit card as your payment mode.Review and complete your order.
                                            </h2>

                                        </div>
                                    </div>
                                    <div>
                                        <button>
                                            Invoice
                                            <span className="float-right" data-toggle="collapse" href="#Invoice" role="button" aria-expanded="false" aria-controls="Invoice">
                                                <img src={filter1} alt="filter"/>
                                            </span>
                                        </button>
                                        <div className="creditcardbtn collapse" id="Invoice">
                                            <h1>
                                                Pay by providing invoice details.
                                            </h1>
                                            <div className="details-order">
                                                <form>
                                                    <div className="form-group">
                                                        <label for="exampleFormControlTextarea1" className="required">Details</label>
                                                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                                    </div>
                                                    <div className="d-flex">
                                                        <div className="form-group">
                                                            <label for="exampleInputEmail1">Purchase Order</label>
                                                            <input type="type" className="form-control" placeholder="Enter purchase Order" />
                                                        </div>
                                                        <div className="form-group">
                                                            <label for="exampleInputEmail1">Client Tax/Vat Number</label>
                                                            <input type="type" className="form-control" placeholder="Enter Client Tax/Vat Number" />
                                                        </div>
                                                    </div>
                                                    <button type="submit" className="btn btn-primary">Submit</button>
                                                </form>


                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div className="col-md-4">
                            <div className="order-summary">
                                <div className="ordersummaryhead">
                                    <h1>
                                        Order Summary
                                    </h1>
                                </div>
                            </div>
                            <div className="complete-orderbnt">
                                <button>
                                    COMPLETE ORDER
                                </button>
                            </div>
                            <div>
                                <div className="cancel-orderbtn">
                                    <button>
                                        CANCEL
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    )
}

export default Paygateway
