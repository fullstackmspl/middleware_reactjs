import React from 'react'
import logo from '../assets/logo.png'
import './footer.css'

const Footer = () => {
    return (
        <div>
            <footer className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 col-lg-6">
                            <div className="widget1">
                                <div className="logo">
                                    <img src={logo} className="img-fluid" alt="" />
                                </div>
                                <p>
                                    In eu libero ligula. Fusce eget metus lorem, ac viverra
                                    leo. Nullam convallis, arcu vel pellentesque sodales,
                                    nisi est varius diam, ac ultrices sem ante quis sem.
                                    Proin ultricies volutpat sapien.
                                </p>
                                <div className="socialLinks">
                                    <ul>
                                        <li>
                                            <a href="fb">
                                                <i className="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="twitter">
                                                <i className="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="linkedin">
                                                <i className="fab fa-linkedin-in"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="google">
                                                <i className="fab fa-google-plus-g"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-6 col-lg-6">
                            <div className="widget3">
                                <h5>
                                    Contact Us
                                </h5>
                                <ul>
                                    <li>
                                        <a href="number">
                                            (+91) 4522. 452. 4545
                                        </a>
                                    </li>
                                    <li>
                                        <a href="content">
                                            danielSupport@gilmoreglobal.com<br />
                                            ersr towerdataside nera polus <br />
                                            Data plus - India<br />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="copyRightArea">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 text-center d-flex">
                                <p>&copy; Copyright All rights reserved 2021.</p>
                                <p>Privacy Policy and Terms of Use </p>
                                <p> Need Help? </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default Footer
