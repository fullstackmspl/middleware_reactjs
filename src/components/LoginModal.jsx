import React,{useState} from 'react'
import loginmodal from '../assets/login-modal.png'
import logo2 from '../assets/logo2.png'
import './loginmodal.css'
// import SignUp from './SignUp'
import './SignUp.css'



const LoginModal = () => {
    const [first, setFirst] = useState("")
    const [last, setLast] = useState("")
    const [email, setEmail] = useState("")
    const [company, setCompany] = useState("")
    const [phone, setPhone] = useState("")
    const [custom, setCustom] = useState("")
    

    

    const signUp =async (e) =>{
        e.preventDefault();

       let userCredentials = {
           first: first,
           last: last,
           email: email,
           company: company,
           phone: phone,
           custom: custom,
           language: "en",
           country: "US"
       };
       try {
           const requestOptions = {
               method: 'POST',
               headers:
               {
                   'Content-Type': 'application/json'
               },
               body: JSON.stringify(userCredentials)
           };
           let result = await fetch("http://93.188.167.68:9001/accounts", requestOptions);
           let respose = await result.json();
           localStorage.setItem("userInfo", JSON.stringify(respose))
           // console.log('result', respose)
       } catch (err) {
           alert("error-" + err)
       }
   }

    

    return (
        <div>
            
                <div className="modal fade" id="login-modal"tabIndex="-1" role="dialog" aria-labelledby="login-modal" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <img className="login-img1" src={loginmodal} alt=""/>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <div className="login-content">
                                <div className="login-content-img">
                                    <img src={logo2} alt=""/>
                                    <h2>Please create a new account here</h2>
                                </div>

                                <div className="login-content-form">
                                    <h1>Welcome to our new learning system! </h1>
                                    <form>
                                        
                                        <div className="form-group">
                                            <input type="text" className="form-control" 
                                            placeholder="First name"
                                            value={first}
                                            onChange={(e)=>setFirst(e.target.value)}
                                             
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" 
                                            className="form-control" 
                                            placeholder="Last name"
                                            value={last} 
                                            onChange={(e)=>setLast(e.target.value)}
                                             />
                                        </div>
                                        <div className="form-group">
                                            <input type="email" 
                                            className="form-control" 
                                            placeholder="Email address" 
                                            value={email}
                                            onChange={(e)=>setEmail(e.target.value)}
                                             />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" 
                                            className="form-control" 
                                            placeholder="Company name"
                                            value={company}
                                            onChange={(e)=>setCompany(e.target.value)} 
                                             />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" 
                                            className="form-control" 
                                            placeholder="Phone no"
                                            value={phone}
                                            onChange={(e)=>setPhone(e.target.value)} 
                                             />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" 
                                            className="form-control" 
                                            placeholder="Custom"
                                            value={custom}
                                            onChange={(e)=>setCustom(e.target.value)} 
                                             />
                                        </div>
                                        
                                        
                                        <p><a href="togle" data-toggle="modal" data-target="#forgot-modal">Forgot Password?</a></p>

                                    </form>
                                </div>

                                <div className="login-content-buttons">
                                    <div>
                                      <button onClick={signUp} data-toggle="modal" data-target="#signup-modal" >SIGN UP</button>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
            
    )
            
}

export default LoginModal
