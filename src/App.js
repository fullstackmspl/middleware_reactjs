import React from 'react'
import Header from './components/Header'
import Routes from './Routes'
import TopHeader from './components/TopHeader'
import LoginModal from './components/LoginModal'

const App = () => {

    return (
        <div>
            <TopHeader/>
            <Header/>
            <Routes/>
            <LoginModal/>
            
        </div>
    )
}

export default App
